Project: e-restaurant "Ahoi"


Clone the repository and cd into the directory, then run composer install form the command line.

Set up the SQLite Database (database file: database.sqlite is available in the directory "darbas"). On .env file set DB_CONNECTION=sqlite and DB_DATABASE=(indicate the full path path to the file (database.sqlite) location, for example: DB_DATABASE=/home/student1v/darbas/database.sqlite). 

After installation has finished, use php artisan serve in the command line to browse the website on localhost:8000.

To test the application, use the following logins:  


admin login: labas@labas.lt; password: 123123
user login: klientas@klientas.lt; password: 123123