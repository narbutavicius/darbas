<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>'auth'], function(){
	Route::get('/orders/user', 'OrdersController@userIndex')->name('orders.user');
	Route::get('/orders/store', 'OrdersController@store')->name('orders.store');
	Route::get('/userprofile', 'UserController@index')->name('profile');
	Route::put('/userprofile', 'UserController@update')->name('updateprofile');
});

Route::group(['middleware'=>['auth', 'admin']], function(){
	Route::resource('/admin/dishes', 'AdminDishController', ['as'=>'admin']);
	Route::resource('/orders', 'OrdersController', ['except'=>['store']]);
	Route::resource('/reservation', 'TableReservationController');
	Route::resource('/adminusers', 'AdminUserController');
});

Route::get('/reservation/create', 'TableReservationController@create')->name('reservation.create');
Route::post('/reservation', 'TableReservationController@store')->name('reservation.store');

Route::get('/home', 'HomeController@index');
Route::get('/dishes', 'DishController@index')->name('dishes_list');
Route::get('/contacts', function (){
	return view('user.contact');
})->name('contacts');
Route::resource('cart', 'CartController');