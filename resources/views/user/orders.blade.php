@extends('layouts.app')

@section('content')

@section ('title', 'Orders')

<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">	
		<a class="btn btn-medium" href="{{route('dishes_list')}}">Return to menu</a>
	</div>
</div>

@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif   

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table2 orders-table">
			<thead>
				<tr>
					<th>User Name</th>
					<th>Date</th>
					<th>Total</th>
					<th>Total with tax</th>
					<th>Order ID</th>
					<th>Customer ID</th>
				</tr>				
			</thead>
			@foreach($orders as $order)
			<tr>
				<td class="table2">{{ $order->user->getFullName() }}</td>	
				<td class="table2">{{ $order->created_at }}</td>
				<td class="table2">{{ $order->total }}</td>	
				<td class="table2">{{ $order->total_with_tax }}
				<td class="table2">{{ $order->id }}</td>
				<td class="table2">{{ $order->user_id }}</td>
			</tr>
			@endforeach
			<tr>
				
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-md-6 thumbnail text-center">
		<div class="bg-success"> Total: {{$orders->sum('total')}} Eur</div>
	</div>
	<div class="col-md-6 thumbnail text-center">
		<div class="bg-success"> Total with tax: {{$orders->sum('total_with_tax')}}	Eur</div>
	</div>	
</div>


@endsection