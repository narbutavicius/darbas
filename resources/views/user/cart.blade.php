@extends('layouts.app')

@section('content')

@section ('title', 'Cart')

@include('partials.errors', ['errors'=>$errors])
<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">	
		<a class="btn btn-medium" href="{{route('dishes_list')}}">Return to menu</a>
	</div>
</div>

<div class="row text-center">
@foreach($dishes->chunk(3) as $chunk)
  <div class="row">
  @foreach($chunk as $dish)
    <div class="col-md-4">
      <div class="thumbnail">
        <img  class="img-thumbnail img-responsive" style = "width:304px; height:236px;" src="{{ $dish->getPhotoUrl() }}">
        <div class="caption">
          <h3>{{$dish->title}}</h3>
          <p>Price: {!!$dish->price!!}</p>
          <p>Quantity: {{$cart->get($dish->id)}}</p>
          <p>Sale price: {!!$dish->getSalePrice()!!}</p>
          <p>Total Price: {{$cart->get($dish->id)*$dish->price}}</p>
          <p>Total Sale Price: {{$cart->get($dish->id)*$dish->getSalePrice()}}</p>
          {!! Form::open(['route' => ['cart.destroy', $dish->id], 'method' => 'DELETE'])!!}
          <p>{!! Form::submit('Remove from cart', ['class'=>'btn btn-warning']) !!}</p>
  		    {!! Form::close() !!}
        </div>
      </div>
    </div>
   @endforeach
  </div>
@endforeach
</div>


<div class="row">
	<div class="col-md-6 col-md-offset-3">
  	<div class="thumbnail">
		<div class="caption">
      <h3>Payment information</h3>
      <p>Total Price: {{$total}}</p>
      <p>Total Sale price: {{$totalSalePrice}}</p>
    	{!! Form::open(['route' =>'orders.store','method' => 'GET'])!!}
	    <p>{!! Form::submit('Order', ['class'=>'btn btn-warning']) !!}</p>
	    {!! Form::close() !!}
   	</div>
	</div>
</div>


@endsection

