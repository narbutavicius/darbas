@extends('layouts.app')

@section ('content')

@section ('title', 'Profile')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="row m-5">
	<div class="col-md-12 text-center">	
		<a class="btn " href="{{route('dishes_list')}}">Return to menu</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table2 profile">
			<thead>
				<tr>
					<th>Name</th>
					<th>Surname</th>
					<th>Email</th>
					<th>Birthdate</th>
					<th>Phone</th>
					<th>Address</th>
					<th>City</th>
					<th>Zipcode</th>
					<th>Country</th>
				</tr>
			</thead>
			<tr>
				{!! Form::model($user, ['route' => ['updateprofile', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				<td>{!! Form::text('name', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('surname', null, ['class'=>'form-control', 'required'=>'required']) !!} </td>
				<td>{!! Form::text('email', null, ['class'=>'form-control', 'required'=>'required']) !!} </td>
				<td>{!! Form::text('birthdate', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('phone', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('address', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('city', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('zipcode', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::text('country', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
				<td>{!! Form::submit('Save', ['class'=>'btn btn-warning']) !!}</td>	
				{!! Form::close() !!}
			</tr>
		</table>	
	</div>
</div>

@endsection