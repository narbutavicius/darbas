@extends('layouts.app')

@section ('content')

@section ('title', 'Menu')

@include('partials.errors', ['errors'=>$errors])

@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif   

<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">	
		<a class="btn btn-medium" href="{{route('cart.index')}}">Basket: {{$cartCount}} item(s)</a>
	</div>
</div>

<div class="row text-center">
@foreach($dishes->chunk(2) as $chunk)
	<div class="row">
	@foreach($chunk as $dish)
	  <div class="col-md-6">
	    <div class="thumbnail">
	    	<div max with="304">
	      <img  class="img-thumbnail img-responsive" style = "width:304px; height:236px;" src="{{ $dish->getPhotoUrl() }}">
	      </div>
	      <div class="caption">
	        <h3>{{$dish->title}}</h3>
	        <p>{!!$dish->description!!}</p>
	        <p>Price: {!!$dish->price!!}</p>
	        <p>Sale price: {!!$dish->getSalePrice()!!}</p>
	        {!! Form::open((['route' => ['cart.update', $dish->id], 'method' => 'PUT']))!!}
	        <p>Quantity: {!! Form::selectRange('quantity', 1, 5) !!}</p>
	        <p>{!! Form::submit('Add to cart', ['class'=>'btn btn-warning'])!!}</p>
			{!! Form::close() !!}
	      </div>
	    </div>
	  </div>
	@endforeach
	</div>
	@endforeach
</div>
			





@endsection