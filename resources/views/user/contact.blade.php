@extends('layouts.app')

@section('content')

@section ('title', 'Contacts')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div>
                <div class="panel panel-primary">
                    <div class="text-center header">Contact Us</div>
                        <div class="panel-body text-center">
                            <p>Restaurant is always ready to delight you with perfectly prepared European dishes, attentive service, and pleasant environment.</p>
    						<p>Professional staff of the restaurant carefully creates a menu for every season and prepares every meal you order with the utmost care. The restaurant offers elaborate gourmet meals inspired by the culinary art innovations and made by our chefs.</p>
    						<p>The restaurant’s interior is designed for your comfort and cosiness. A spacious, light, and modern environment allows you to relax and leisurely enjoy quality food.</p>
    						<p>You are kindly welcome to the restaurant every day and on your special occasions. The restaurant can serve around 160 guests at a time. It is an ideal venue for a business lunch or dinner, as well as for private occasions such as wedding parties, anniversary parties, and major public holidays.</p>
                                                    
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <div class="panel panel-primary">
                    <div class="text-center header">Our Office</div>
                        <div class="panel-body text-center">
                            <h4>Address</h4>
                            <div>
                            Svitrigailos 5<br />
                            Vilnius<br />
                            #(8 5) 12345<br />
                            ahoi@ahoy.lt<br />
                            </div>
                            <hr />
                            <div id="map" class="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  function initMap() {
    var uluru = {lat: 54.679333, lng: 25.266333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
  }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYdE-0PSN8EQ-FIHU11O_l3NfVSc9qRaU&callback=initMap">
</script>

@endsection