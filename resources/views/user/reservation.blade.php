@extends('layouts.app')

@include('partials.errors', ['errors'=>$errors])

@section('content')

@section ('title', 'Reservation')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-warning">
                <div class="panel-heading text-center">Reserve a table</div>
                    <div class="panel-body">                 
        		{!! Form::open(['route'=>'reservation.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif   
                
                    <div class="form-group">
                       
                        <label class="col-sm-4 control-label">Name</label>
                       		<div class="col-sm-6">                                 
    						{{ Form::text('name', $name, ['class'=>'form-control', 'required'=>'required', 'placeholder'=>'Name'])}} 
    						</div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Number of persons</label>
                          	<div class="col-sm-6">                              
                          	{!! Form::selectRange('persons', 1, 10, 1, ['class'=>'form-control']) !!}
                          	</div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-6">                           
                            {!! Form::text('date', null, ['required'=>'required', 'class'=>'form-control', 'placeholder'=>'yyyy-mm-dd'])!!}
                     		</div>
                    </div>

                    <div class="form-group"sm>
                        <label class="col-sm-4 control-label">Time</label>
                        	<div class="col-sm-6">  
                            {!! Form::time('time', null, ['required'=>'required', 'class'=>'form-control'])!!}
                      		</div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Phone</label>
    					    <div class="col-sm-6">                             
                            {!! Form::text('phone', $phone, ['required'=>'required', 'class'=>'form-control'])!!}
                       		</div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-4">                            
                {!! Form::submit('Book', ['class'=>'form-control btn btn-warning']) !!}                           
                        </div>
                    </div>
                {!! Form::close() !!}           
                </div>
            </div>
        </div>
    </div>
</div>

@endsection