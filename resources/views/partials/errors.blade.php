@section('error')

@foreach($errors->all() as $message)
	<div class="alert alert-danger">{{$message}}</div>
@endforeach

@endsection