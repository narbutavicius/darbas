@extends('layouts.app')


@section('content')

@section ('title', 'Create reservation')

@include('partials.errors', ['errors'=>$errors])

<div class="row m-5">
	<div class="col-md-6 text-center">	
		<p>Welcome, Admin</p>
	</div>
	<div class="col-md-6 text-center">	
		<a class="btn btn-primary btn-medium" href="{{route('reservation.create')}}">Create new reservation</a>
	</div>
</div>

<div class="row m-5">
	<table class="table table-hover">
		<tr>
			<th>Reservation ID</th>
			<th>Name</th>
			<th>Number of persons</th>
			<th>Date</th>
			<th>Time</th>
			<th>Phone</th>
			<th></th>
		</tr>
		<tr>
			{!! Form::open(['route'=>'reservation.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}

			<td>{{ Form::text('name', null, ['class'=>'form-control', 'required'=>'required'])}} </td>
			<td>{!! Form::selectRange('persons', 1, 10, 1, ['class'=>'form-control']) !!}</td>
			<td>{!! Form::date('date', null, ['required'=>'required', 'class'=>'form-control'])!!}</td>		
			<td>{!! Form::time('time', null, ['required'=>'required', 'class'=>'form-control'])!!}</td>
			<td>{!! Form::text('phone', null, ['required'=>'required', 'class'=>'form-control'])!!}</td>			
			<td>{!! Form::submit('Create', ['class'=>'btn btn-warning']) !!}<td>
			{!! Form::close() !!}
		</tr>
	</table>	
</div>


@endsection