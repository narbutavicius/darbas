@extends('layouts.app')

@section('content')

@section ('title', 'List of reservations')

@include('partials.errors', ['errors'=>$errors])

<div class="row m-5">
	<div class="col-md-6 text-center">	
		<p>Welcome, {{ Auth::user()->name }}</p>
	</div>
	<div class="col-md-6 text-center">	
		<a class="btn btn-primary btn-medium" href="{{route('reservation.create')}}">Create new reservation</a>
	</div>
</div>

<div class="row m-5">
	<table class="table table-hover table2 tablelist">
		<thead>
			<th>Reservation ID</th>
			<th>Name</th>
			<th>Date</th>
			<th>Time</th>
			<th>Phone</th>
			<th>Persons</th>
			<th></th>
			<th></th>
		</thead>

	@foreach($reservations as $reservation)
		<tr>
			<td>{{$reservation->id}}</td>
			<td>{{$reservation->name}}</td>
			<td>{!!$reservation->date!!}</td>		
			<td>{!!$reservation->time!!}</td>
			<td>{!!$reservation->phone!!}</td>
			<td>{!!$reservation->persons!!}</td>
			<td> 
				<a class="btn btn-warning" href="{{route('reservation.edit', $reservation->id)}}">Edit </a>
			</td>
			<td>
				{!! Form::open(['route' => ['reservation.destroy', $reservation->id], 'method' => 'DELETE'])!!}
				{!! Form::submit('Delete', ['class'=>'btn btn-warning']) !!}
				{!! Form::close() !!}
			<td>
		</tr>
		@endforeach
	</table>
</div>


@endsection