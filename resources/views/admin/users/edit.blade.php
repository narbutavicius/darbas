@extends('layouts.app')

@section('content')

@section ('title', 'Edit user')

@include('partials.errors', ['errors'=>$errors])


{!! Form::model($user, ['route' => ['adminusers.update', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

<table class="table table-hover table2 profile">
	<thead>
		<th>Name</th>
		<th>Surname</th>
		<th>Email</th>
		<th>Birthdate</th>
		<th>Phone</th>
		<th>Address</th>
		<th>City</th>
		<th>Zipcode</th>
		<th>Country</th>
		<th></th>
	</thead>
	
	<tr>
		<td>{!! Form::text('name', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('surname', null, ['class'=>'form-control', 'required'=>'required']) !!} </td>
		<td>{!! Form::text('email', null, ['class'=>'form-control', 'required'=>'required']) !!} </td>
		<td>{!! Form::text('birthdate', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('phone', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('address', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('city', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('zipcode', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::text('country', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
		<td>{!! Form::submit('Save', ['class'=>'btn btn-warning']) !!}</td>
	</tr>
</table>	

{!! Form::close() !!}


@endsection

