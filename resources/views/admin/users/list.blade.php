@extends('layouts.app')

@section('content')

@section ('title', 'List of users')

@include('partials.errors', ['errors'=>$errors])


<div class="row m-5">
	<div class="col-md-6 text-center">	
		<p>Welcome, {{ Auth::user()->name }}</p>
	</div>

	<div class="col-md-6 text-center">	
		<a class="btn btn-primary btn-medium" href="{{ route('adminusers.create') }}">Create new User</a>
	</div>
</div>

<div class="row m-5">
	<table class="table table-hover table2 profileadmin">
		<thead>
			<th>Name</th>
			<th>Surname</th>
			<th>User ID</th>
			<th>Email</th>
			<th>Birthdate</th>
			<th>Phone</th>
			<th>Address</th>
			<th>City</th>
			<th>Zipcode</th>
			<th>Country</th>
			<th></th>
		</thead>

	@foreach($users as $user)
		<tr>
			<td>{{$user->name}}</td>
			<td>{{$user->surname}}</td>
			<td>{{$user->id}}</td>
			<td>{!!$user->email!!}</td>		
			<td>{!!$user->birthdate!!}</td>
			<td>{!!$user->phone!!}</td>
			<td>{!!$user->address!!}</td>
			<td>{!!$user->city!!}</td>
			<td>{!!$user->zipcode!!}</td>
			<td>{!!$user->country!!}</td>
	@if($user->id != Auth::user()->id)
			<td> 
				<a class="btn btn-warning" href="{{route('adminusers.edit', [$user->id])}}">Edit </a>
			</td>
			<td>
				{!! Form::open(['route' => ['adminusers.destroy', $user->id], 'method' => 'DELETE'])!!}
				{!! Form::submit('Delete', ['class'=>'btn btn-warning']) !!}
				{!! Form::close() !!}
			<td>
	@else
			<td colspan="2"></td>
	@endif
		</tr>
	@endforeach
	</table>	
</div>

@endsection

