@extends('layouts.app')

@section('content')

@section ('title', 'List of orders')

@include('partials.errors', ['errors'=>$errors])

<div class="row m-5">
	<div class="col-md-6 text-center">	
		<p>Welcome, {{ Auth::user()->name }}</p>
	</div>

	<div class="col-md-6 text-center">	
		<a class="btn btn-primary btn-medium" href="{{route('dishes_list')}}">Return to menu</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">	
			<table class="table table-hover table2 list-table">
				<thead>					
					<th>User Name</th>
					<th>User ID</th>
					<th>Date</th>
					<th>Total</th>
					<th>Total with tax</th>
					<th>Order ID</th>					
				</thead>
					@foreach($orders as $order)
				</tr>
					{!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'DELETE'])!!}
					<td>@if($order->user){{ $order->user->getFullName() }} @else no name @endif</td>
					<td>{{ $order->user_id }}</td>
					<td>{{ $order->created_at }}</td>
					<td>{{ $order->total }}</td>	
					<td>{{ $order->total_with_tax }}
					<td>{{ $order->id }}</td>
					<td>{!! Form::submit('Delete order', ['class'=>'btn btn-warning']) !!}<td>
					{!! Form::close() !!}
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 thumbnail text-center">
		<div class="bg-success"> Total: {{$orders->sum('total')}} Eur</div>
	</div>
	<div class="col-md-6 thumbnail text-center">
		<div class="bg-success"> Total with tax: {{$orders->sum('total_with_tax')}}	Eur</div>
	</div>	
</div>


@endsection