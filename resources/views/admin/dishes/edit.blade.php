@extends('layouts.app')


@section ('content')

@section ('title', 'Edit dish')

@include('partials.errors', ['errors'=>$errors])

{!! Form::model($dish, ['route' => ['admin.dishes.update', $dish->id], 'method' => 'PUT', 'files'=> true, 'class' => 'form-horizontal']) !!}

<div class="form-group">                            
    <label class="col-sm-4 control-label">Title</label>
   		<div class="col-sm-6">                                 
		{!! Form::text('title', null, ['required'=>'required', 'class'=>'form-control'])!!} 
		</div>
</div>

<div class="form-group">                            
    <label class="col-sm-4 control-label">Description</label>
   		<div class="col-sm-6">                                 
		{!! Form::textarea('description') !!}
		</div>
</div>

<div class="form-group">                            
    <label class="col-sm-4 control-label">Quantity</label>
   		<div class="col-sm-6">                                 
		{!! Form::selectRange('quantity', 1, 5) !!}
		</div>
</div>

<div class="form-group">                            
    <label class="col-sm-4 control-label">Price</label>
   		<div class="col-sm-6">                                 
		{!! Form::text('price', null, ['required'=>'required', 'class'=>'form-control'])!!}
		</div>
</div>

<div class="form-group">                            
    <label class="col-sm-4 control-label">Sale Price</label>
   		<div class="col-sm-6">                                 
		{!!$dish->getSalePrice()!!}
		</div>
</div>

<div class="form-group">                            
    <label class="col-sm-4 control-label">Sale Price</label>
   		<div class="col-sm-6">                                 
		{!! Form::file('photo') !!}
		</div>
</div>

<div class="form-group">
    <div class="col-sm-6 col-sm-offset-4">   
		<img class="responsive img-thumbnail" style = "width:304px; height:236px;" src="{{ $dish->getPhotoUrl() }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-6 col-sm-offset-4">      
	{!! Form::submit('Save', ['class'=>'btn btn-warning']) !!}
    </div>
</div>

{!! Form::close() !!}


@endsection