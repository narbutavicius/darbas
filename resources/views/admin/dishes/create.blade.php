@extends('layouts.app')


@section ('content')

@section ('title', 'Create dish')

@include('partials.errors', ['errors'=>$errors])

<div class="table-responsive">
	<table class="table table-hover table2">
		<thead>
			<th>Title</th>
			<th>Description</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Photo</th>
			<th></th>
		</thead>

		<tr class="createdish">
		{!! Form::open(['route'=> 'admin.dishes.store', 'files' => true, 'class' => 'form-horizontal']) !!}
			<td>{!! Form::text('title', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
			<td>{!! Form::textarea('description', null, ['class'=>'form-control']) !!} </td>
			<td>{!! Form::selectRange('quantity', 1, 10, 1, ['class'=>'form-control']) !!} </td>
			<td>{!! Form::text('price', null, ['class'=>'form-control', 'required'=>'required'])!!}</td>
			<td>{!! Form::file('photo', ['class'=>'form-control']) !!}</td>
			<td>{!! Form::submit('Create', ['class'=>'btn btn-success']) !!}</td>
			{!! Form::close() !!}
		</tr>
	</table>
</div>
@endsection

 