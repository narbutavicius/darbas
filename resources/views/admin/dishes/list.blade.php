@extends('layouts.app')

@include('partials.errors', ['errors'=>$errors])

@section ('content')

@section ('title', 'List of dishes')

<div class="row m-5">
	<div class="col-md-6 text-center">	
		<p>Welcome, {{ Auth::user()->name }}</p></p>
	</div>

	<div class="col-md-6 text-center">	
		<a class="btn btn-primary btn-medium" href="{{route('admin.dishes.create')}}">Create new dish</a>
	</div>
</div>

<div class="row m-5">
	<table class="table table-hover text-center table2">
		<thead>
			<th class="text-center">ID</th>
			<th class="text-center">Title</th>
			<th class="text-center">Description</th>
			<th class="text-center">Quantity</th>
			<th class="text-center">Price</th>
			<th class="text-center">Sale Price</th>
			<th class="text-center">Photo</th>
			<th></th>
			<th></th>
			<th></th>
		</thead>
				
		@foreach($dishes as $dish)
		<tr class="dishlist">
			<td>{{$dish->id}}</td>
			<td>{{$dish->title}}</td>
			<td>{!!$dish->description!!}</td>		
			<td>{!! Form::selectRange('quantity', 1, 5) !!} </td>
			<td>{!!$dish->price!!}</td>
			<td>{!!$dish->getSalePrice()!!}</td>
			<td>
				<img  class="img-thumbnail img-responsive" style = "width:304px; height:236px;" src="{{ $dish->getPhotoUrl() }}">
			</td>
			<td> 
				<a class="btn btn-warning" href="{{route('admin.dishes.edit', [$dish->id])}}">Edit </a>
			</td>
			<td>
				{!! Form::open(['route' => ['admin.dishes.destroy', $dish->id], 'method' => 'DELETE'])!!}
				{!! Form::submit('Delete', ['class'=>'btn btn-warning']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</table>
</div>
@endsection