<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Restaurant Ahoi</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background: url({{$url = asset('/images/bgr.jpg')}});
              /*  background-color: #fff;*/
                color: white;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            header{
                background-color: rgba(0, 0, 0, 0.5);
                z-index: 100;

            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                font-weight: 600;
            }

            .title {
                font-size: 60px;
            }

            .links > a {
                /*olor: #636b6f;*/
                color: white;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="flex-center position-ref full-height">
                @if (Route::has('login'))
                    <div class="top-right links">
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    </div>            
                @endif

                <div class="content">
                    <div class="title m-b-md">
                        Restaurant Ahoi!!!
                    </div>

                    <div class="links">

                        <a href="{{ route('dishes_list') }}">Dishes menu</a>
                        <a href="{{ route('reservation.create') }}">Table reservation</a>
                        <a href="{{ route('contacts') }}">Contacts</a>
                        
                    </div>
                </div>
            </div>
        </header>
    </body>
</html>
