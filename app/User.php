<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Order;
use App\Cart;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'birthdate', 'phone', 'address', 'city', 'zipcode', 'country' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'admin'
    ];

    public function orders(){
        return $this->belongsToMany(App\Order::class);
    }

    public function getFullName(){
        return $this->name . ' ' . $this->surname;
    }

    public function isAdmin(){
        return (bool) $this->admin;
    }
}

