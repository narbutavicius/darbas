<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = ['title', 'description', 'quantity', 'price', 'saleprice', 'photo'];

    public function getPhotoUrl(){
    	return asset('uploads/' . $this->photo);
    }

    public function getSalePrice(){
    	return $this->price * 1.21;
    }
}

