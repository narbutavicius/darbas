<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderDish extends Model
{
    protected $fillable = [
    'order_id', 'dish_id', 'quantity', 'price'];

    public function order(){
    	return $this->belongsTo(App\Order::class);
    }

    public function  dish(){
    	return $this->belongsTo(App\Dish::class);
    }
}
