<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cart;

class Order extends Model
{
	protected $fillable = [
	'user_id', 'total'];

    public function user (){
    	return $this->belongsTo('App\User');
    }

    public function dishes(){
    	return $this->belongsToMany('App\Dish')->withPivot('price', 'quantity');
    }

    public function getTotalWithTax(){
        $dishes = $this->dishes;
        $total = 0;
        
        foreach($dishes as $dish){
            $total +=  $dish->pivot->quantity * $dish->pivot->price * 1.21;
        }
        
        $total = number_format($total, 2);
        return $total;
    }

    public function getTotalWithTaxAttribute(){
        return $this->getTotalWithTax();
    }

}


