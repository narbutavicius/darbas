<?php

namespace App\Http\Middleware;
use Illuminate\Auth\AuthenticationException;
use Request;

class Authenticate extends \Illuminate\Auth\Middleware\Authenticate {


    protected function authenticate (array $guards){
        try{
            return parent::authenticate($guards);
        } catch(AuthenticationException $e){
            $url = Request::fullUrl();
            session()->put('lastUrl', $url);
            throw $e;
        }
    }
}