<?php

namespace App\Http\Requests;

class StoreDishEdit extends StoreDish
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['photo'] = 'image';

        return $rules;
    }
}
