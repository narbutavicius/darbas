<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dish;
use App\Http\Controllers\CartController;
use App\Cart;



class DishController extends Controller
{
   

	public function index(Request $request){

	$cart = $request->session()->get('cart', new Cart());
	$cartCount = $cart->count();
   $dishes = Dish::all();
   return view('user.menu', compact('dishes', 'cartCount'));
	}
}
