<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dish;
use App\Http\Requests\StoreDish;
use App\Http\Requests\StoreDishEdit;

class AdminDishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = Dish::all();
        return view('admin.dishes.list', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dishes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDish $request)
    {
        $file =  $request->file('photo');
        $dir = public_path('uploads');

        $filename = $this->generateFileName($dir, $file);
        $request-> file('photo')->move($dir, $filename);
            
        $dish = Dish::create($request->all());
        $dish->update(['photo' => $filename]);
 
        return redirect()->route('admin.dishes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dish = Dish::find($id);

                
        return view('admin.dishes.edit', compact('dish'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDishEdit $request, $id)
    {
        $dish = Dish::find($id);
       
        $dish -> update($request->all());

        if($request->hasFile('photo')){
            $file =  $request->file('photo');
            $dir = public_path('uploads');
            $filename = $this->generateFileName($dir, $file);
            $request-> file('photo')->move($dir, $filename);
            $dish->update(['photo' => $filename]);
        }
       return redirect()->route('admin.dishes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dish = Dish::destroy($id);
        return redirect()
        ->route('admin.dishes.index');
    }

    protected function generateFileName($dir, $file){
        
        $originalName = $file->getClientOriginalName();
        $parts = explode('.', $originalName);
        $extension = $parts[count($parts) - 1];

        while(true){
            $filename = md5(microtime(true)) . '.' . $extension;
            if(!file_exists($dir . '/' . $filename)){
                return $filename;
            }
        }
    }
}
