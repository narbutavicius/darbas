<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use App\User;
use Auth;


class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('user.userprofile', compact('user'));
    }

  

    public function update(StoreUser $request)
    {
        $user = Auth::user();
        $user->update($request->all());
        return redirect()->route('profile')
        				->withStatus('Profile updated');
    }
}
