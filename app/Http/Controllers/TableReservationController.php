<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TableReservation;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreReservation;

class TableReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = TableReservation::latest()->get();
        return view('admin.tables.list', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = old('name');
        $phone = old('phone');

        if (Auth::check()){
            if(!$name){
                $name = Auth::user()->getFullName();
            }

            if(!$phone){
                $phone = Auth::user()->phone;
            }
        }

        return view('user.reservation', compact('name', 'phone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservation $request)
    {
        $user = Auth::user();
        $reservation = TableReservation::create($request->all());
        $redirectTo = ($user && $user ->isAdmin()) ? 'reservation.create':'contacts';
        return redirect()->route($redirectTo)->withStatus('Booked!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservation = TableReservation::find($id);

                
        return view('admin.tables.edit', compact('reservation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreReservation $request, $id)
    {
        $reservation = TableReservation::find($id);
        $reservation -> update($request->all());
        return redirect()->route('reservation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = TableReservation::destroy($id);
        return redirect()->route('reservation.index');

    }
}
