<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Dish;
use App\Order;
// use 

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'DESC')->get();

        return view('admin.orders.list', compact('orders')); 
    }

    public function userIndex()
    {
        
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return  view('user.orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = Cart::getCart();
        if ($cart->count()== 0) return redirect()->back();

        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->total = $cart->getTotal();
        $order->save();

        $cart->each(function ($qty, $dishId) use ($order) {
            $dish = Dish::find($dishId);
            $order->dishes()->attach($dishId, ['quantity' => $qty, 'price' => $dish->price]);
        });

        $cart->reset();
       
        return redirect()->route('dishes_list')->withMessage('Order done');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::destroy($id);
        return redirect()->route('orders.index', compact('orders'));
    }
}
