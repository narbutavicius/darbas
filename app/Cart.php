<?php

namespace App;


use Illuminate\Support\Collection;

class Cart extends Collection
{
    protected $items = [];

	public function addItem($dishId, $quantity){
		if(array_key_exists($dishId, $this->items)){
			// $this->items[$product->id] = $item;
			$this->items[$dishId] += $quantity;
		} else{
			$this->items[$dishId] = $quantity;
		}	
	}

	public function getDishes(){
		$dishes = Dish::whereIn('id', $this->keys())->get();
		return $dishes;
	}

	public function getTotal(){
		$dishes = $this->getDishes();
		$total = 0;
		
		foreach($dishes as $dish){

            $total += $this->get($dish->id) * $dish->price;

           
        }
        $total = number_format($total, 2);
        return $total;
    }

    public function getTotalSalePrice(){
    	$dishes = $this->getDishes();
		$total = 0;
		foreach($dishes as $dish){

            $total += $this->get($dish->id) * $dish->getSalePrice();
           
        }

        $total = number_format($total, 2);

        return $total;
    }

    public static function getCart(){
    	return session('cart', new Cart());

    } 

    public function save(){
    	session()->put('cart', $this);
    	
    	return $this;
    }

    public function reset()
    {
    	session()->put('cart', new static);
    
    }

    public function calculateDishPrice($dish){
        return $this->get($dish->id)*$dish->price;
    }
}
